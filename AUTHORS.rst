=======
Credits
=======

Development Lead
----------------

* Etienne Lac <etienne.lac@safrangroup.com>
* Duc Trung Le <duc-trung.le@safrangroup.com>

Contributors
------------

* Adrien Delsalle <adrien.delsalle@safrangroup.com>
* Alexis Cassier <alexis.cassier@safrangroup.com>
* Amath Waly Ndiaye <amath-waly.ndiaye@safrangroup.com>
* Frederic Collonval <frederic.collonval@safrangroup.com>
* Guy De Spiegeleer <guy.de-spiegeleer@safrangroup.com>
* Thomas Federici <thomas.federici@safrangroup.com>
