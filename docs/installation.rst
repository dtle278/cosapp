.. highlight:: shell

============
Installation
============

Stable release
--------------

The easiest way is to install the conda package:

.. code-block:: console

    conda install cosapp -c conda-forge

or the PyPi package:

.. code-block:: console

    pip install cosapp

From sources
------------

The sources for CoSApp can be downloaded from the `Gitlab repo`_.

You can either clone the public repository:

.. code-block:: console

    git clone https://gitlab.com/cosapp/cosapp.git

Or download the `archive`_:

.. code-block:: console

    curl -OL https://gitlab.com/cosapp/cosapp/repository/master/archive.zip

Once you have a copy of the source, you can install it with:

.. code-block:: console

    python -m pip install .


.. _Gitlab repo: https://gitlab.com/CoSApp/cosapp
.. _archive: https://gitlab.com/cosapp/cosapp/repository/master/archive.zip
