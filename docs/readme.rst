.. image:: ./img/website-docs-blue.svg
    :alt: website
    :target: https://cosapp.readthedocs.io
.. image:: ./img/gitlab-cosapp-f4950f.svg
    :alt: gitlab
    :target: https://gitlab.com/cosapp/cosapp
    
.. include:: ../README.rst
