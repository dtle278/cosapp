{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "nbsphinx": "hidden"
   },
   "source": [
    "![CoSAppLogo](images/cosapp.svg) **CoSApp** tutorials: Data visibility"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Visibility ![Experimental feature](images/experimental.svg)\n",
    "\n",
    "The people using models (i.e. `System` in the case of **CoSApp**) are usually not the one who develops the model. Therefore the user may not be aware of the model limits and not all of the model parameters will be meaningful for him. \n",
    "\n",
    "To address those issues, **CoSApp** let the model developer specify validity range and visibility on all variables. This section will address the visibility feature.\n",
    "\n",
    "\n",
    "## The concept\n",
    "\n",
    "Three levels of visibility are available in **CoSApp**:\n",
    "\n",
    "* `PUBLIC`: Everybody can set those variables\n",
    "* `PROTECTED`: Only advanced and expert user can set those variables\n",
    "* `PRIVATE`: Only expert user can set those variables\n",
    "\n",
    "The accessibility is the intersection between the `System` tags and the `User` role:\n",
    "\n",
    "* `PUBLIC`: no shared tag\n",
    "* `PROTECTED`: more than one shared tag (but not all)\n",
    "* `PRIVATE`: same tags list\n",
    "\n",
    "![visibility](../tutorials/images/visibility.svg)\n",
    "\n",
    "## Example\n",
    "\n",
    "Let consider the mechanical and aerodynamic design of a blade by three users; a mechanical, a aerodynamic and a system engineer.\n",
    "\n",
    "The `System`s for doing so will have the following inward variables:\n",
    "\n",
    "```python\n",
    "class MechanicalBlade(System):\n",
    "    \n",
    "    tags = ['blade', 'mechanics']\n",
    "    \n",
    "    def setup(self):\n",
    "        self.add_inward('height', 0.4, scope=Scope.PUBLIC)\n",
    "        self.add_inward('thickness', 0.01, scope=Scope.PROTECTED)\n",
    "        self.add_inward('material', 'steel')  # Scope will be PRIVATE by default\n",
    "        \n",
    "class AerodynamicBlade(System):\n",
    "    \n",
    "    tags = ['blade', 'aerodynamic']\n",
    "    \n",
    "    def setup(self):\n",
    "        self.add_inward('height', 0.4, scope=Scope.PUBLIC)\n",
    "        self.add_inward('thickness', 0.01, scope=Scope.PROTECTED)\n",
    "        self.add_inward('camber', 1e-3)  # Scope will be PRIVATE by default\n",
    "        \n",
    "class Blade(System):\n",
    "    \n",
    "    tags = ['blade', 'integration']\n",
    "    \n",
    "    def setup(self):\n",
    "        self.add_child(MechanicalBlade('mechanics'), pulled={'height': 'height'})\n",
    "        self.add_child(AerodynamicBlade('aerodynamic'), pulled={'height': 'height'})\n",
    "```\n",
    "\n",
    "The user will have the following role:\n",
    "\n",
    "| User | Role |\n",
    "|---|---|\n",
    "| Mechanical Engineer | [\"blade\", \"mechanics\"] |\n",
    "| Aerodynamic Engineer | [\"blade\", \"aerodynamic\"] |\n",
    "| System Engineer | [\"integration\"] |\n",
    "\n",
    "\n",
    "So the visibility for the variables will be:\n",
    "\n",
    "| User | MechanicalBlade | AerodynamicBlade | Blade |\n",
    "|---|---|---|---|\n",
    "| Mechanical Engineer | PRIVATE | PROTECTED | PROTECTED |\n",
    "| Aerodynamic Engineer | PROTECTED | PRIVATE | PROTECTED |\n",
    "| System Engineer | PUBLIC | PUBLIC | PROTECTED |\n",
    "\n",
    "\n",
    "## Defining visibility\n",
    "\n",
    "The example highlight the link between the user role and its ability to set a variable.\n",
    "\n",
    "User roles are currently saved in a configuration file (*%USERPROFILE%\\\\.cosapp.d\\cosapp_config.json* on Windows and *\\$HOME/.cosapp.d/cosapp_config.json* on Unix). A role is defined as a group of tags. For example the aerodynamic engineer will have the following roles: \n",
    "\n",
    "```json\n",
    "{\n",
    "  \"roles\" : [\n",
    "    [\"aerodynamic\", \"rotor\"],\n",
    "    [\"aerodynamic\", \"stator\"]\n",
    "  ]\n",
    "}\n",
    "```\n",
    "\n",
    "To determine the variable visibility for a given user, its roles will be compared to the `tags` of each `System`. If one role matches exactly the tags, the user will have the status of `PRIVATE` on the `System`. If one tag in one role matches at least one system tag, the user will have the status of `PROTECTED`. Otherwise the user will have `PUBLIC` access.\n",
    "\n",
    "As for the validity ranges, the visibility is set in `Port` or `System` object. But it is not possible to change a `Port` variable visibility inside a `System` on the opposite of validity parameters.\n",
    "\n",
    "For example in a `Port`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbsphinx": "hidden"
   },
   "outputs": [],
   "source": [
    "import logging\n",
    "logging.getLogger().setLevel(logging.INFO)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.systems import System\n",
    "from cosapp.ports import Port, Scope, ScopeError\n",
    "\n",
    "class MyPort(Port):\n",
    "    \n",
    "    def setup(self):\n",
    "        self.add_variable('v', 22.)  # Scope will be PUBLIC by default\n",
    "        self.add_variable('w', 22., scope=Scope.PRIVATE)\n",
    "        self.add_variable('x', 22., scope=Scope.PROTECTED)\n",
    "        self.add_variable('y', 22., scope=Scope.PUBLIC)        "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As `Port` are the doors for exchanging information between `System`, it should not be common to define a scope on them. Therefore their default visibility is `PUBLIC`.\n",
    "\n",
    "And in a `System`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class MechanicalBlade(System):\n",
    "    \n",
    "    tags = ['blade', 'mechanics']  # Tags must be specified to activate visibility, otherwise all variables will be open.\n",
    "    \n",
    "    def setup(self):\n",
    "        self.add_inward('height', 0.4, scope=Scope.PUBLIC)\n",
    "        self.add_inward('thickness', 0.01, scope=Scope.PROTECTED)\n",
    "        self.add_inward('material', 'steel')  # Scope will be PRIVATE by default\n",
    "        \n",
    "        port_in = self.add_input(MyPort, 'port_in')\n",
    "        # Visibility of variable v in port_in cannot be modified\n",
    "        \n",
    "        port_out = self.add_output(MyPort, 'port_out')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The *inwards* are the preferred place to define variables with restrained visibility. Therefore their default scope is `PRIVATE`.\n",
    "\n",
    "## Displaying visibility\n",
    "\n",
    "To obtain the documentation of a *System* or a *Port*, you can use the utility function `display_doc`. \n",
    "\n",
    "Variables with `PRIVATE` scope will be marked with 🔒🔒 and `PROTECTED` ones with 🔒."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.tools import display_doc\n",
    "\n",
    "display_doc(MyPort)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "display_doc(MechanicalBlade)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Testing visibility\n",
    "\n",
    "If a variable has a scope different than `PUBLIC`, user won't be able to set it except if they have the right role.\n",
    "\n",
    "As a `System` is defining its *outputs* and *outwards* from its *inputs* and *inwards*. Only the two latter will be protected as the former will be overwrite during the component calculation.\n",
    "\n",
    "So hoping you are not having the role ['blade', 'runner'], here are the variables you can not touch."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "BladeRunner = MechanicalBlade  # Class duplication\n",
    "BladeRunner.tags = ['blade', 'runner']  # Changing the tags on the new class\n",
    "\n",
    "b = BladeRunner('Ridley')\n",
    "\n",
    "from warnings import warn\n",
    "\n",
    "try:\n",
    "    b.thickness = 0.01\n",
    "except ScopeError as err:\n",
    "    warn(\"ScopeError raised: {}\".format(err))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "try:\n",
    "    b.material = 0.01\n",
    "except ScopeError as err:\n",
    "    warn(\"ScopeError raised: {}\".format(err))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "try:\n",
    "    b.port_in.w = 0.01\n",
    "except ScopeError as err:\n",
    "    warn(\"ScopeError raised: {}\".format(err))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "But visibility on output port is not enforced:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "b.port_out.w = 0.01\n",
    "print(b.port_out)"
   ]
  }
 ],
 "metadata": {
  "hide_input": false,
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.7"
  },
  "toc": {
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": "block",
   "toc_window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
