{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "nbsphinx": "hidden"
   },
   "source": [
    "![CoSAppLogo](images/cosapp.svg) **CoSApp** tutorials: Simulation logging system"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Simulation log\n",
    "\n",
    "CoSApp simulation logger is built on top of the Python built-in package `logging`. To set the simulation log, you will use the helper function `set_log`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.utils import LogLevel, set_log\n",
    "\n",
    "help(set_log)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Example\n",
    "\n",
    "The following cell creates the two tanks problem (see [Transient simulations](TimeDriver.ipynb) tutorial for more information). The usage of `set_log` will be demonstrated on it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "from cosapp.ports import Port\n",
    "from cosapp.systems import System\n",
    "from cosapp.drivers import EulerExplicit, NonLinearSolver\n",
    "\n",
    "\n",
    "class FloatPort(Port):\n",
    "    def setup(self):\n",
    "        self.add_variable('value', 0.0)\n",
    "\n",
    "\n",
    "class Tank(System):\n",
    "    def setup(self, rho=1e3):\n",
    "        self.add_inward('area', 1.0, desc='Cross-section area')\n",
    "        self.add_inward('rho', abs(rho), desc='Fluid density')\n",
    "\n",
    "        self.add_input(FloatPort, 'flowrate')\n",
    "        self.add_output(FloatPort, 'p_bottom')\n",
    "\n",
    "        self.add_transient('height', der='flowrate.value / area')\n",
    "\n",
    "    def compute(self):\n",
    "        g = 9.81\n",
    "        self.p_bottom.value = self.rho * g * self.height\n",
    "\n",
    "\n",
    "class Pipe(System):\n",
    "    \"\"\"Poiseuille flow in a cylindrical pipe\"\"\"\n",
    "    def setup(self):\n",
    "        self.add_inward('D', 0.1, desc=\"Diameter\")\n",
    "        self.add_inward('L', 2.0, desc=\"Length\")\n",
    "        self.add_inward('mu', 1e-3, desc=\"Fluid dynamic viscosity\")\n",
    "\n",
    "        self.add_input(FloatPort, 'p1')\n",
    "        self.add_input(FloatPort, 'p2')\n",
    "\n",
    "        self.add_output(FloatPort, 'Q1')\n",
    "        self.add_output(FloatPort, 'Q2')\n",
    "\n",
    "        self.add_outward('k', desc='Pressure loss coefficient')\n",
    "\n",
    "    def compute(self):\n",
    "        \"\"\"Computes the volumetric flowrate from the pressure drop\"\"\"\n",
    "        self.k = np.pi * self.D**4 / (256 * self.mu * self.L)\n",
    "        self.Q1.value = self.k * (self.p2.value - self.p1.value)\n",
    "        self.Q2.value = -self.Q1.value\n",
    "\n",
    "\n",
    "class CoupledTanks(System):\n",
    "    \"\"\"System describing two tanks connected by a pipe (viscous limit)\"\"\"\n",
    "    def setup(self, rho=1e3):\n",
    "        self.add_child(Tank('tank1', rho=rho))\n",
    "        self.add_child(Tank('tank2', rho=rho))\n",
    "        self.add_child(Pipe('pipe'))\n",
    "\n",
    "        self.connect(self.tank1.p_bottom, self.pipe.p1)\n",
    "        self.connect(self.tank2.p_bottom, self.pipe.p2)\n",
    "        self.connect(self.tank1.flowrate, self.pipe.Q1)\n",
    "        self.connect(self.tank2.flowrate, self.pipe.Q2)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## `set_log` usage"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Default usage\n",
    "\n",
    "In its default usage, the simulation log will be stored in a file with the default name in the current folder. Only messages of level `LogLevel.INFO` or above will be displayed."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "set_log()\n",
    "\n",
    "system = CoupledTanks(\"coupledTanks\", rho=1e3)\n",
    "driver = system.add_driver(EulerExplicit(dt=0.1, time_interval=[0, 0.1]))\n",
    "\n",
    "solver = driver.add_child(NonLinearSolver(\"solver\", factor=1.0))\n",
    "\n",
    "h1_0, h2_0 = (3, 1)\n",
    "\n",
    "driver.set_scenario(\n",
    "    name=\"run\",\n",
    "    init={\"tank1.height\": h1_0, \"tank2.height\": h2_0,},  # initial conditions\n",
    "    values={\"pipe.D\": 0.07, \"pipe.L\": 2.5, \"tank1.area\": 2,},  # fixed values\n",
    ")\n",
    "\n",
    "system.run_drivers()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Set log level\n",
    "\n",
    "If you want to have more of less information, you could set the level of message to be recorded according to the following scale:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "LogLevel?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "set_log(level=LogLevel.WARNING) # less information (none in this case)\n",
    "\n",
    "system = CoupledTanks(\"coupledTanks\", rho=1e3)\n",
    "driver = system.add_driver(EulerExplicit(dt=0.1, time_interval=[0, 0.1]))\n",
    "\n",
    "solver = driver.add_child(NonLinearSolver(\"solver\", factor=1.0))\n",
    "\n",
    "h1_0, h2_0 = (3, 1)\n",
    "\n",
    "driver.set_scenario(\n",
    "    name=\"run\",\n",
    "    init={\"tank1.height\": h1_0, \"tank2.height\": h2_0,},  # initial conditions\n",
    "    values={\"pipe.D\": 0.07, \"pipe.L\": 2.5, \"tank1.area\": 2,},  # fixed values\n",
    ")\n",
    "\n",
    "system.run_drivers()  # Should no emit any log message"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "set_log(level=LogLevel.DEBUG) # more information\n",
    "\n",
    "system = CoupledTanks(\"coupledTanks\", rho=1e3)\n",
    "driver = system.add_driver(EulerExplicit(dt=0.1, time_interval=[0, 0.1]))\n",
    "\n",
    "solver = driver.add_child(NonLinearSolver(\"solver\", factor=1.0))\n",
    "\n",
    "h1_0, h2_0 = (3, 1)\n",
    "\n",
    "driver.set_scenario(\n",
    "    name=\"run\",\n",
    "    init={\"tank1.height\": h1_0, \"tank2.height\": h2_0,},  # initial conditions\n",
    "    values={\"pipe.D\": 0.07, \"pipe.L\": 2.5, \"tank1.area\": 2,},  # fixed values\n",
    ")\n",
    "\n",
    "system.run_drivers()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Add time filter\n",
    "\n",
    "With level lower than `DEBUG`, the number of message recorded starts to be important. But usually, when debugging, you may be interested by only looking at debug message only from a later time. You could achieve that by specifying the `start_time` keyword:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "set_log(level=LogLevel.DEBUG, start_time=0.1)  # Be more verbose for time later than 0.1s\n",
    "\n",
    "system = CoupledTanks(\"coupledTanks\", rho=1e3)\n",
    "driver = system.add_driver(EulerExplicit(dt=0.1, time_interval=[0, 0.1]))\n",
    "\n",
    "solver = driver.add_child(NonLinearSolver(\"solver\", factor=1.0))\n",
    "\n",
    "h1_0, h2_0 = (3, 1)\n",
    "\n",
    "driver.set_scenario(\n",
    "    name=\"run\",\n",
    "    init={\"tank1.height\": h1_0, \"tank2.height\": h2_0,},  # initial conditions\n",
    "    values={\"pipe.D\": 0.07, \"pipe.L\": 2.5, \"tank1.area\": 2,},  # fixed values\n",
    ")\n",
    "\n",
    "system.run_drivers()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Add context filter\n",
    "\n",
    "Another possibility would be to filter by context; i.e. by systems or by driver.\n",
    "\n",
    "> When filtering by systems, the mentionned system and all its children will be part of the more verbose log. See for example the log for the dummy subsystem `subtank1` in the following example."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "set_log(level=LogLevel.DEBUG, context=\"tank1\") # Filter on a System\n",
    "\n",
    "system = CoupledTanks(\"coupledTanks\", rho=1e3)\n",
    "driver = system.add_driver(EulerExplicit(dt=0.1, time_interval=[0, 0.1]))\n",
    "\n",
    "system.tank1.add_child(System(\"subtank1\"))\n",
    "solver = driver.add_child(NonLinearSolver(\"solver\", factor=1.0))\n",
    "\n",
    "h1_0, h2_0 = (3, 1)\n",
    "\n",
    "driver.set_scenario(\n",
    "    name=\"run\",\n",
    "    init={\"tank1.height\": h1_0, \"tank2.height\": h2_0,},  # initial conditions\n",
    "    values={\"pipe.D\": 0.07, \"pipe.L\": 2.5, \"tank1.area\": 2,},  # fixed values\n",
    ")\n",
    "\n",
    "system.run_drivers()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "set_log(level=LogLevel.DEBUG, context=\"solver\") # Filter on a Driver\n",
    "\n",
    "system = CoupledTanks(\"coupledTanks\", rho=1e3)\n",
    "driver = system.add_driver(EulerExplicit(dt=0.1, time_interval=[0, 0.1]))\n",
    "\n",
    "solver = driver.add_child(NonLinearSolver(\"solver\", factor=1.0))\n",
    "\n",
    "h1_0, h2_0 = (3, 1)\n",
    "\n",
    "driver.set_scenario(\n",
    "    name=\"run\",\n",
    "    init={\"tank1.height\": h1_0, \"tank2.height\": h2_0,},  # initial conditions\n",
    "    values={\"pipe.D\": 0.07, \"pipe.L\": 2.5, \"tank1.area\": 2,},  # fixed values\n",
    ")\n",
    "\n",
    "system.run_drivers()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## `FULL_DEBUG` mode\n",
    "\n",
    "The `DEBUG` mode provides a trace of the calls done by the solver within the simulation system. But when troubles arise, nothing is more valuable than values to analyse a simulation. This is the purpose of the `FULL_DEBUG` mode. Its behavior will depend of the object. What you could expect for now is:\n",
    "\n",
    "- For all `System`: Record the values of the inputs (including the inwards) and the outputs (including the outwards)\n",
    "- For the `NonLinearSolver`: Record the computed Jacobian matrix and the evolution of the unknowns and the residues. Those informations are printed as comma-separated table to ease their post-process; e.g. to plot a graph of the evolution.\n",
    "- For all `TimeDriver`: Record the time steps used."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Records for `System`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "set_log(level=LogLevel.FULL_DEBUG, context=\"tank1\", start_time=0.1)\n",
    "\n",
    "system = CoupledTanks(\"coupledTanks\", rho=1e3)\n",
    "driver = system.add_driver(EulerExplicit(dt=0.1, time_interval=[0, 0.1]))\n",
    "\n",
    "solver = driver.add_child(NonLinearSolver(\"solver\", factor=1.0))\n",
    "\n",
    "h1_0, h2_0 = (3, 1)\n",
    "\n",
    "driver.set_scenario(\n",
    "    name=\"run\",\n",
    "    init={\"tank1.height\": h1_0, \"tank2.height\": h2_0,},  # initial conditions\n",
    "    values={\"pipe.D\": 0.07, \"pipe.L\": 2.5, \"tank1.area\": 2,},  # fixed values\n",
    ")\n",
    "\n",
    "system.run_drivers()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Records for `NonLinearSolver`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "set_log(level=LogLevel.FULL_DEBUG, context=\"solver\", start_time=0.1)\n",
    "\n",
    "system = CoupledTanks(\"coupledTanks\", rho=1e3)\n",
    "driver = system.add_driver(EulerExplicit(dt=0.1, time_interval=[0, 0.1]))\n",
    "\n",
    "solver = driver.add_child(NonLinearSolver(\"solver\", factor=1.0))\n",
    "\n",
    "h1_0, h2_0 = (3, 1)\n",
    "\n",
    "driver.set_scenario(\n",
    "    name=\"run\",\n",
    "    init={\"tank1.height\": h1_0, \"tank2.height\": h2_0,},  # initial conditions\n",
    "    values={\"pipe.D\": 0.07, \"pipe.L\": 2.5, \"tank1.area\": 2,},  # fixed values\n",
    ")\n",
    "\n",
    "system.run_drivers()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Records for `TimeDriver`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "set_log(level=LogLevel.FULL_DEBUG, context=\"euler\")\n",
    "\n",
    "system = CoupledTanks(\"coupledTanks\", rho=1e3)\n",
    "driver = system.add_driver(EulerExplicit(\"euler\", dt=0.1, time_interval=[0, 0.2]))\n",
    "\n",
    "solver = driver.add_child(NonLinearSolver(\"solver\", factor=1.0))\n",
    "\n",
    "h1_0, h2_0 = (3, 1)\n",
    "\n",
    "driver.set_scenario(\n",
    "    name=\"run\",\n",
    "    init={\"tank1.height\": h1_0, \"tank2.height\": h2_0,},  # initial conditions\n",
    "    values={\"pipe.D\": 0.07, \"pipe.L\": 2.5, \"tank1.area\": 2,},  # fixed values\n",
    ")\n",
    "\n",
    "system.run_drivers()"
   ]
  }
 ],
 "metadata": {
  "language_info": {
   "name": "python",
   "pygments_lexer": "ipython3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
