{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "nbsphinx": "hidden"
   },
   "source": [
    "![CoSAppLogo](images/cosapp.svg) **CoSApp** tutorials:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Drivers\n",
    "\n",
    "## What is a `Driver`?!\n",
    "\n",
    "A `Driver` object represents a simulation you want to run on your `System`. This can be the mathematical\n",
    "resolution of non-linear system or an optimization for example.\n",
    "\n",
    "\n",
    "## Introduction\n",
    "\n",
    "### Add a `Driver`\n",
    "\n",
    "Simply use the `add_driver` method passing the `Driver` object you want to use \n",
    "(see [Available drivers](#Available-Drivers) section of this tutorial)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.tests.library.systems import Multiply1\n",
    "from cosapp.drivers import RunOnce\n",
    "\n",
    "m = Multiply1('mult')\n",
    "run = m.add_driver(RunOnce('run', verbose=True))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`Multiply1` *is the same* `System` *as* `Multiply` *in the* [Systems](01-Systems.ipynb) *tutorial*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    " ![Driver in system](images/drivers_1.svg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Implementation\n",
    "\n",
    "Every `System` can have one or multiple `Driver` objects. They are stored in the `drivers` attribute.\n",
    "By default, there is no `Driver` attached to a `System`.\n",
    "\n",
    "The `run_drivers` method of `System` executes its drivers. In the following example, the simplest driver\n",
    "[RunOnce](#RunOnce) will be added and then executed."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.tests.library.systems import Multiply1\n",
    "\n",
    "m = Multiply1('mult')\n",
    "m.add_driver(RunOnce('run'))\n",
    "print(m.drivers) # print drivers of the system\n",
    "\n",
    "m.p_in.x = 15.\n",
    "m.K1 = 2.\n",
    "m.run_drivers()\n",
    "print(m.p_out.x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Subdrivers - Similarities with `System`\n",
    "\n",
    "A `Driver` can have children which also inherit from `Driver`, they are kind of numerical systems. It allows more complex simulation such as workflow, multipoints design, design of experiments, optimization, etc.\n",
    "\n",
    "By construction, a `System` can have n-levels of drivers. These children are stored in the `children` attribute of `Drivers`. Use the `add_child` method of a `Driver`.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![drivers](images/drivers_2.svg)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "m = Multiply1('mult')\n",
    "run = m.add_driver(RunOnce('run', verbose=True))\n",
    "\n",
    "print(run.children) # the 'run' driver has no child\n",
    "\n",
    "subrun = run.add_child(RunOnce('subrun')) # add a child `Driver` called 'subrun'\n",
    "print(run.children, subrun.children)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Available Drivers\n",
    "\n",
    "**CoSApp** comes with a bunch of `Drivers` to allow users building their simulations\n",
    "\n",
    "### RunOnce\n",
    "\n",
    "It makes your `System` and its subsystems compute their code. It does not deal with residues or iterative loops that may be necessary to resolve the `System`.\n",
    "\n",
    "Let's define a new `System` that has a residue for the purpose of this section."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.systems import System\n",
    "from cosapp.tests.library.ports import XPort\n",
    "\n",
    "class MultiplyWithResidue(System):\n",
    "\n",
    "    def setup(self):\n",
    "        self.add_input(XPort, 'p_in', {'x': 1.})\n",
    "        self.add_inward('K1', 5.)\n",
    "        # expected_output will be used as a target for p_out.x variable\n",
    "        self.add_inward('expected_output', 7.5)\n",
    "        self.add_output(XPort, 'p_out', {'x': 1.})\n",
    "        \n",
    "        # create a new equation: expected_output = p_out.x\n",
    "        self.add_equation('expected_output == p_out.x') \n",
    "\n",
    "    def compute(self):\n",
    "        self.p_out.x = self.p_in.x * self.K1\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then test this `Driver`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.drivers import RunOnce\n",
    "\n",
    "m = MultiplyWithResidue('mult')\n",
    "run = m.add_driver(RunOnce('run', verbose=True))\n",
    "\n",
    "m.run_drivers()\n",
    "\n",
    "print('List of defined drivers\\n', m.drivers)\n",
    "print('\\np_in.x\\tK1\\tp_out.x\\tresidue')\n",
    "print('{}\\t{}\\t{}\\t{}'.format(m.p_in.x, m.K1, m.p_out.x, m.residues.values()))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### NonLinearSolver\n",
    "\n",
    "It resolves your `System` residues playing with free parameters. This `Driver` resolves the mathematical problem between free parameters and residues of his children drivers.\n",
    "By default, it comes with a [RunSingleCase](#RunSingleCase) child, called *runner*. \n",
    "\n",
    "Note : this `Driver` does not compute the `System`. So the recommended structure is to add a `RunSingleCase` driver to\n",
    "a non-linear solver. And then to customized it to be able to solve the system."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![scipysolver](images/drivers_nonlinear.svg)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.drivers import NonLinearSolver\n",
    "\n",
    "m = MultiplyWithResidue('mult')\n",
    "solver = m.add_driver(NonLinearSolver('solver', verbose=True))\n",
    "solver.runner.offdesign.add_unknown('p_in.x')\n",
    "m.run_drivers()\n",
    "\n",
    "print(m.drivers)\n",
    "print(m.drivers['solver'].children)\n",
    "print(m.p_in.x, m.K1, m.expected_output, m.p_out.x, m.residues.values())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### RunSingleCase\n",
    "\n",
    "It executes all subsystems drivers by calling `compute` method of each `children`. It also computes the `System` itself if it has code defined in the `compute` method.\n",
    "\n",
    "This `Driver` does not have a solver but is helpfull to set boundary conditions, initial values and additional equations (see [Advanced Drivers](./03b-Advanced-Drivers.ipynb) tutorial).\n",
    "\n",
    "To update its `System` owner, 4 possibilities are offered:\n",
    "\n",
    "- *value modification* through `set_values` method will change an input value\n",
    "- *initial value definition* through `set_init` method will change the value of iteratives\n",
    "before resolving the case."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.drivers import RunSingleCase\n",
    "\n",
    "m = MultiplyWithResidue('mult')\n",
    "update = m.add_driver(RunSingleCase('update', verbose=True))\n",
    "update.offdesign.add_unknown('p_in.x')\n",
    "\n",
    "update.set_values({'expected_output': 15.})\n",
    "update.set_init({'K1': 2.})\n",
    "m.run_drivers()\n",
    "\n",
    "print('List of defined drivers\\n', m.drivers)\n",
    "print('\\np_in.x\\tK1\\tp_out.x\\tresidue')\n",
    "print('{}\\t{}\\t{}\\t{}'.format(m.p_in.x, m.K1, m.p_out.x, m.residues.values()))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![updatesystem](images/drivers_4.svg)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "m.drivers.clear() # Remove all drivers on the system `m`\n",
    "solver = m.add_driver(NonLinearSolver('solver', verbose=True))\n",
    "update = solver.add_child(RunSingleCase('update', verbose=True))\n",
    "update.offdesign.add_unknown('p_in.x')\n",
    "\n",
    "# Customization of the case\n",
    "update.set_values({'expected_output': 15.})\n",
    "\n",
    "# Execution\n",
    "m.run_drivers()\n",
    "\n",
    "print(m.drivers)\n",
    "print(m.p_in.x, m.K1, m.expected_output, m.p_out.x, m.residues.values())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Congrats!** You are now ready to launch computation on your `System` with **CoSApp**!\n",
    "\n",
    "But we recommend you to read the [Advanced Drivers](./03b-Advanced-Drivers.ipynb) tutorial, before \n",
    "creating your test case."
   ]
  }
 ],
 "metadata": {
  "language_info": {
   "name": "python",
   "pygments_lexer": "ipython3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
