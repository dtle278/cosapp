{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "nbsphinx": "hidden"
   },
   "source": [
    "![CoSAppLogo](images/cosapp.svg) **CoSApp** tutorials: Data validation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Validation\n",
    "\n",
    "The people using models (i.e. `System` in the case of **CoSApp**) are usually not the one who develops the model. Therefore the user may not be aware of the model limits and not all of the model parameters will be meaningful for him. \n",
    "\n",
    "To address those issues, **CoSApp** let the model developer specify validity range and visibility on all variables. This section focus on the validation feature.\n",
    "\n",
    "\n",
    "## The concept\n",
    "\n",
    "A validity range and a limit range can be defined on all variables in **CoSApp**.\n",
    "\n",
    "![gauge](images/validity.svg)\n",
    "\n",
    "The validity range defines a range of values for which the model can be used with confidence. The yellow areas between the validity range and the limits define values for which a validation by an expert is required. And the values beyond the limits should be avoided as the reliability of the model is unknown.\n",
    "\n",
    "Those ranges are given as information. Therefore their respect is not ensured and should be checked after a model execution. This behavior has been chosen to limit the constraints on the mathematical systems to be solved.\n",
    "\n",
    "## Defining validation criteria\n",
    "\n",
    "**CoSApp** variables are defined in `Port` or in `System` object. So in those two places validity range can be specified.\n",
    "\n",
    "For example in a `Port`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbsphinx": "hidden"
   },
   "outputs": [],
   "source": [
    "import logging\n",
    "logging.getLogger().setLevel(logging.INFO)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.systems import System\n",
    "from cosapp.ports import Port\n",
    "from cosapp.drivers import ValidityCheck\n",
    "\n",
    "class MyPort(Port):\n",
    "    \n",
    "    def setup(self):\n",
    "        self.add_variable('v', 22., \n",
    "                          valid_range = (-2, 5),\n",
    "                          invalid_comment = 'design rule abc forbids \"a\" outside [-2, 5]',\n",
    "                          limits = (-10, None),\n",
    "                          out_of_limits_comment = 'The model has not been tested outside [-10, [')\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And in a `System`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class MySystem(System):\n",
    "    \n",
    "    def setup(self):\n",
    "        # Definition of validation criteria on a inward variable 'd'\n",
    "        self.add_inward('d', 7., \n",
    "                      valid_range = (-2, 5),\n",
    "                      invalid_comment = 'design rule abc forbid \"a\" outside [-2, 5]',\n",
    "                      limits = (None, 10),\n",
    "                      out_of_limits_comment = 'The model has not been tested outside ], 10]')\n",
    "        \n",
    "        # Overwrite the default validation criteria on the variable 'v' of port 'port_in'\n",
    "        port_in = self.add_input(MyPort, 'port_in', \n",
    "                                 {'v': dict(  # Variable name in MyPort\n",
    "                                     valid_range = (0, 3),\n",
    "                                     invalid_comment = 'design rule efg forbids \"a\" outside [0, 3]',\n",
    "                                     limits = (None, 10),\n",
    "                                     out_of_limits_comment = 'The model has not been tested outside ], 10]')})"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If no range are provided, the variable are always considered as valid. In case the value is not valid but is within the limits, the `invalid_comment` will be shown to the user to inform him on the reason of the validity range. And if the value is out of the limits, the `out_of_limits_comment` will be displayed.\n",
    "\n",
    "If one side of the range is unbounded, the user will specify `None` as bound.\n",
    "\n",
    "## Displaying validation criteria\n",
    "\n",
    "You can get those information by displaying the documentation of the `Port` or `System`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.tools import display_doc\n",
    "\n",
    "display_doc(MyPort)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "display_doc(MySystem)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Testing validation criteria\n",
    "\n",
    "As said earlier, the validity ranges are not forced during the simulation. Therefore to check the validity of the results after execution, you have to add a special driver `ValidityCheck` to the master `System` to have a report on the data validity.\n",
    "\n",
    "Values that fall outside the limits will be gathered in the `ERROR` section and those between the valid range and the limits will be shown in the `WARNING` section. Valid values won't be shown."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = MySystem('master_system')\n",
    "s.add_driver(ValidityCheck('validation'))\n",
    "\n",
    "s.run_drivers()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Let see it in practice\n",
    "\n",
    "We will demonstrate those concept on the [circuit example](aa-SimpleCircuit.ipynb).\n",
    "\n",
    "The physical problem is to determine the voltage on the two nodes to equilibrate intensity.\n",
    "\n",
    "![simple-circuit](images/simple_circuit.svg)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.systems import System\n",
    "from cosapp.ports import Port, Scope, ScopeError\n",
    "from cosapp.drivers import NonLinearSolver, NonLinearMethods, ValidityCheck\n",
    "import numpy as np"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Set validity criteria\n",
    "\n",
    "By default, the current intensity will be defined as positive."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbsphinx": "hidden"
   },
   "outputs": [],
   "source": [
    "class Voltage(Port):\n",
    "    \n",
    "    def setup(self):\n",
    "        self.add_variable('V')\n",
    "\n",
    "        \n",
    "class Intensity(Port):\n",
    "    \n",
    "    def setup(self):\n",
    "        self.add_variable('I', limits=(0., None),\n",
    "                          out_of_limits_comment=\"Current can only flow in one direction.\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In *Resistor* component, we add a validity range on the current intensity pretending an non-constant behavior of the resistance value for high current. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbsphinx": "hidden"
   },
   "outputs": [],
   "source": [
    "class Resistor(System):\n",
    "    \n",
    "    def setup(self, R = 1.):\n",
    "        self.add_input(Voltage, 'V_in')\n",
    "        self.add_input(Voltage, 'V_out')\n",
    "        I = self.add_output(Intensity, 'I',\n",
    "                            {'I': dict( \n",
    "                                valid_range=(0., 25.),\n",
    "                                invalid_comment='For high current the resistance may not be constant.')})\n",
    "        \n",
    "        self.add_inward('R', R, desc='Resistance in Ohms')\n",
    "        self.add_outward('deltaV')\n",
    "        \n",
    "    def compute(self):\n",
    "        self.deltaV = self.V_in.V - self.V_out.V\n",
    "        self.I.I = self.deltaV / self.R\n",
    "        "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can look at the documentation to check that the validation criteria are taken into account"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.tools import display_doc\n",
    "display_doc(Resistor)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Building the circuit"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hide_input": false,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "class Diode(System):\n",
    "    \"\"\"Diode model\n",
    "    \n",
    "    The current intensity flowing through the diode is calculated based on\n",
    "\n",
    "    $ I = I_s \\\\exp \\\\left( \\\\dfrac{V_{in} - V_{out}}{V_t} - 1 \\\\right) $\n",
    "    \"\"\"\n",
    "    tags = ['cosapp', 'developer']\n",
    "    \n",
    "    def setup(self):\n",
    "        self.add_input(Voltage, 'V_in')\n",
    "        self.add_input(Voltage, 'V_out')\n",
    "        self.add_output(Intensity, 'I')\n",
    "        \n",
    "        self.add_inward('Is', 1e-15, desc='Saturation current in Amps')\n",
    "        self.add_inward('Vt', .025875, scope=Scope.PROTECTED, desc='Thermal voltage in Volts')\n",
    "        \n",
    "        self.add_outward('deltaV')\n",
    "        \n",
    "    def compute(self):\n",
    "        self.deltaV = self.V_in.V - self.V_out.V\n",
    "        self.I.I = self.Is * np.exp(self.deltaV / self.Vt - 1.)\n",
    "        \n",
    "\n",
    "class Node(System):\n",
    "    \n",
    "    def setup(self, n_in=1, n_out=1):\n",
    "        self.add_inward('n_in', n_in, limits=(1, None), out_of_limits_comment='Node needs at least one current flowing in.')\n",
    "        self.add_inward('n_out', n_out, limits=(1, None), out_of_limits_comment='Node needs at least one current flowing out.')\n",
    "\n",
    "        for i in range(n_in):\n",
    "            self.add_input(Intensity, 'I_in{}'.format(str(i)))\n",
    "        for i in range(n_out):\n",
    "            self.add_input(Intensity, 'I_out{}'.format(str(i)))\n",
    "        \n",
    "        self.add_inward('V')\n",
    "        self.add_unknown('V')  # Iterative variable\n",
    "        \n",
    "        self.add_outward('sum_I_in', 0., desc='Sum of all input currents')\n",
    "        self.add_outward('sum_I_out', 0., desc='Sum of all output currents')\n",
    "        \n",
    "        self.add_equation('sum_I_in == sum_I_out', name='V')\n",
    "        \n",
    "    def compute(self):\n",
    "        self.sum_I_in = 0.\n",
    "        self.sum_I_out = 0.\n",
    "        \n",
    "        for i in range(self.n_in):\n",
    "            self.sum_I_in += self['I_in{}.I'.format(str(i))]\n",
    "        for i in range(self.n_out):\n",
    "            self.sum_I_out += self['I_out{}.I'.format(str(i))]\n",
    "        \n",
    "        \n",
    "class Source(System):\n",
    "    \n",
    "    def setup(self, I = 0.1):\n",
    "        self.add_inward('I', I)\n",
    "        self.add_output(Intensity, 'I_out', {'I': I})\n",
    "    \n",
    "    def compute(self):\n",
    "        self.I_out.I = self.I\n",
    "        \n",
    "        \n",
    "class Ground(System):\n",
    "    \n",
    "    def setup(self, V = 0.):\n",
    "        self.add_inward('V', V)\n",
    "        self.add_output(Voltage, 'V_out', {'V': V})\n",
    "    \n",
    "    def compute(self):\n",
    "        self.V_out.V = self.V\n",
    "        \n",
    "\n",
    "class Circuit(System):\n",
    "    \n",
    "    def setup(self):\n",
    "        n1 = self.add_child(Node('n1', n_in=1, n_out=2), pulling={'I_in0': 'I_in'})\n",
    "        n2 = self.add_child(Node('n2'))\n",
    "        \n",
    "        R1 = self.add_child(Resistor('R1', R=100.), pulling={'V_out': 'Vg'})\n",
    "        R2 = self.add_child(Resistor('R2', R=10000.))\n",
    "        D1 = self.add_child(Diode('D1'), pulling={'V_out': 'Vg'})  \n",
    "        \n",
    "        self.connect(R1.V_in, n1.inwards, 'V')\n",
    "        self.connect(R2.V_in, n1.inwards, 'V')\n",
    "        self.connect(R1.I, n1.I_out0)\n",
    "        self.connect(R2.I, n1.I_out1)\n",
    "        \n",
    "        self.connect(R2.V_out, n2.inwards, 'V')\n",
    "        self.connect(D1.V_in, n2.inwards, 'V')\n",
    "        self.connect(R2.I, n2.I_in0)\n",
    "        self.connect(D1.I, n2.I_out0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Solving the valid problem\n",
    "\n",
    "First we will solve the initial problem with a intensity source of 0.1 A and check that the results are valid."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "p = System('model')\n",
    "\n",
    "# Plug the source, the ground and the circuit\n",
    "p.add_child(Source('source', I=0.1))\n",
    "p.add_child(Ground('ground', V=0.))\n",
    "p.add_child(Circuit('circuit'))\n",
    "\n",
    "p.connect(p.source.I_out, p.circuit.I_in)\n",
    "p.connect(p.ground.V_out, p.circuit.Vg)\n",
    "\n",
    "# Add numerical solver and validation\n",
    "p.add_driver(NonLinearSolver('solver', method=NonLinearMethods.POWELL))\n",
    "p.add_driver(ValidityCheck('validation'))\n",
    "\n",
    "# Execute the problem\n",
    "p.run_drivers()\n",
    "\n",
    "# sanity check: should sum to -.1 Amps\n",
    "print('Sanity check : 0.1 = ? ', p['circuit.R1.I.I'] + p['circuit.D1.I.I'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Solving invalid problems\n",
    "\n",
    "Then we will solve the problem with a high intensity will be set to trigger validation criteria on the resistors."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "p = System('model')\n",
    "\n",
    "# Plug the source, the ground and the circuit\n",
    "p.add_child(Source('source', I=50.))\n",
    "p.add_child(Ground('ground', V=0.))\n",
    "p.add_child(Circuit('circuit'))\n",
    "\n",
    "p.connect(p.source.I_out, p.circuit.I_in)\n",
    "p.connect(p.ground.V_out, p.circuit.Vg)\n",
    "\n",
    "# Add numerical solver and validation\n",
    "p.add_driver(NonLinearSolver('solver', method=NonLinearMethods.POWELL))\n",
    "p.add_driver(ValidityCheck('validation'))\n",
    "\n",
    "# Init to help the numerical solver\n",
    "p.circuit.n1.V = 1000\n",
    "\n",
    "# Execute the problem\n",
    "p.run_drivers()\n",
    "\n",
    "# sanity check\n",
    "print('Sanity check : 50. = ? ', p['circuit.R1.I.I'] + p['circuit.D1.I.I'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally we will solve the problem with a intensity source of -0.1 A. That value is not consider has valid."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "p = System('model')\n",
    "\n",
    "# Plug the source, the ground and the circuit\n",
    "p.add_child(Source('source', I=-0.1))\n",
    "p.add_child(Ground('ground', V=0.))\n",
    "p.add_child(Circuit('circuit'))\n",
    "\n",
    "p.connect(p.source.I_out, p.circuit.I_in)\n",
    "p.connect(p.ground.V_out, p.circuit.Vg)\n",
    "\n",
    "# Add numerical solver and validation\n",
    "p.add_driver(NonLinearSolver('solver', method=NonLinearMethods.POWELL))\n",
    "p.add_driver(ValidityCheck('validation'))\n",
    "\n",
    "# Execute the problem\n",
    "p.run_drivers()\n",
    "\n",
    "# sanity check: should sum to -0.1 Amps\n",
    "print('Sanity check : -0.1 = ? ', p['circuit.R1.I.I'] + p['circuit.D1.I.I'])"
   ]
  }
 ],
 "metadata": {
  "hide_input": false,
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.7"
  },
  "toc": {
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": "block",
   "toc_window_display": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
