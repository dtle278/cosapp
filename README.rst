CoSApp - Collaborative System Approach
======================================

The primary goal of **CoSApp** is to help technical departments in the design of complex systems.
To do so, the framework allows the simulation of various systems representing the different
parts of the final product in a common environment. The consequences are the ability for each
subsystem team to carry out design study with a direct feedback of the impact of parameters at
the product level.

The main features are :

- Butterfly effect

Coupled your preferred simulation software with CoSApp to get immediate impact on main product
variables and iterates to converge on a better design.

- Design guidance

All systems can share design parameters associated with an acceptable range. You can take advantage
of those limited degrees of freedom without fear of breaking your neighbors' work.

- Margins & Uncertainties

All design parameters have an intrinsic dispersion. Knowing the range of fluctuations is crucial to
ensure the robustness of the design. CoSApp handles natively uncertain variables.

Have a look a the `introduction <https://cosapp.readthedocs.io/en/stable/tutorials/00-Introduction.html#>`_.

This code is the property of Safran SA. It uses code coming from various open-source projects. See `LICENSE <https://gitlab.com/cosapp/cosapp/blob/master/LICENSE.rst>`_ file.
